#!/usr/bin/env Rscript

library(parallel)
library(rtracklayer)
library(ensembldb)
library(EnsDb.Hsapiens.v86)
library(Biostrings)
library(biomaRt)

args = commandArgs(trailingOnly=TRUE)
# test if there is at least one argument: if not, return an error
if (length(args)==0) {
  stop("At least two argument must be supplied (input file, UCSC chain file).n", call.=FALSE)
} else if (length(args)==2) {
  # default output file
  args[3] = "out.txt"
}


# UCSC chain file to convert b/w genome versions  using LiftOver, downloaded from
# https://hgdownload.cse.ucsc.edu/goldenpath/hg19/liftOver/hg19ToHg38.over.chain.gz
path = args[2]
ch = import.chain(path)

# setup params for biomart query
ensembl = useMart("ensembl",dataset="hsapiens_gene_ensembl")

#setup params for ensembldb
edb <- EnsDb.Hsapiens.v86
seqlevelsStyle(edb) <- "UCSC"

# read input data
data <- read.csv(file=args[1], header=FALSE, sep="\t")
colnames(data) <- c("CHROM", "POS", "ID", "REF", "ALT","QUAL", "FILTER", "PRED")

lif_coords <- function(chr_19, pos_19){
    # generate a GRange object from the hg19 coordinates
    gnm_pos <- GRanges(chr_19, IRanges(pos_19, width = 1))
    
    # liftOver GRCh37/hg19 -> GRCh38/hg38
    gnm_pos38 <- liftOver(gnm_pos, ch)[[1]]
    chr_38 <- as.character(seqnames(gnm_pos38))
    pos_38 <- start(gnm_pos38)
    ## Restrict queries to correct chromosome to speed up
    edb <- filter(EnsDb.Hsapiens.v86, filter = ~ seq_name == chr_38)
    seqlevelsStyle(edb) <- "UCSC"
    return(list(gnm_pos38, chr_38, pos_38, edb))
}

map_to_prot <- function(gnm_pos38, edb) {
    # map a genome position to a protein position (Irange)
    prt_pos <- genomeToProtein(gnm_pos38, edb)
    # check output, exit if unable to map 
    out_cols <- c('start', 'end', 'width', 'ENSP_id', 'ENST_id', 'cds_ok')
    if(length(prt_pos[[1]]) == 0) {
	    print("Failed to map genome coords to ENSP")
	    prt_pos_df <- data.frame(matrix(data=NA, ncol=6, nrow=1))
	    colnames(prt_pos_df) <- out_cols
	    return(list(prt_pos_df,prt_pos[[1]]))}

    prt_pos_df <- as.data.frame(prt_pos[[1]])
    # remove positions on non-Ensembl transcript Ids
    # as they are unlikely to map to PDB structures further on
    prt_pos_df$ENST_id <- mcols(prt_pos[[1]])$tx_id
    prt_pos_df$cds_ok <- mcols(prt_pos[[1]])$cds_ok
    prt_pos_df <- prt_pos_df[grep('ENSP', prt_pos_df$names), ]
    # filter out positions falling outside a coding sequence
    # see above
    prt_pos_df <- prt_pos_df[prt_pos_df$cds_ok == TRUE, ]
    if(nrow(prt_pos_df) == 0){
            print("No ENSP retained after filtering")
	    prt_pos_df <- data.frame(matrix(data=NA, ncol=6, nrow=1))
	    colnames(prt_pos_df) <- out_cols
	    return(list(prt_pos_df,prt_pos[[1]]))
    }
    colnames(prt_pos_df) <- out_cols
    return(list(prt_pos_df, prt_pos[[1]]))
}

get_AA_seq <- function(prt_pos_df, edb) {
    # obtain full AA sequence for accepted ENSP ids
    prt_seq <- proteins(edb, return.type = "AAStringSet",
                        filter = ~ protein_id == prt_pos_df$ENSP_id)
    prt_seq_df <- as.data.frame(prt_seq)
    colnames(prt_seq_df) <- c("prt_seq")
    prt_seq_df$ENSP_id <- rownames(prt_seq_df)
    out_df <- merge(prt_pos_df, prt_seq_df, on='ENSP_id')
    # check if protein positions fall inside AA sequence
    out_df <- out_df[nchar(out_df$prt_seq) >= as.numeric(out_df$end), ]
    return(out_df)
}

get_AA <- function(p, seq){
    s <- NA
    seq <- as.character(seq)
    try(s <- base::strsplit(seq, "")[[1]][p])
    return(s)
}


map_to_codon <- function(prt_pos, prt_pos_df, edb) {
    # map a protein AA to corresponding codon
    # in coding sequence, this is slow and produces inconsistencies
    gen_pos <- proteinToGenome(prt_pos[prt_pos_df$ENSP_id], edb)
    arr <- list()
    i <- 1
    # from genome codon to transcript codon then to exon codon
    for (gr in gen_pos){
        trs_pos <- genomeToTranscript(gr, edb)[[1]]
        cds_pos <- transcriptToCds(trs_pos, edb)
        cds_pos <- subset(cds_pos, width(cds_pos) == 3)
        df <- as.data.frame(cds_pos)
        arr[i] = list(df)
        i <- i +1 
    }
    cds_pos_df <- do.call(rbind, arr)
    cds_pos_df <- unique(cds_pos_df[c("start", "end", "width", "names")])
    colnames(cds_pos_df) <- c("trs_start", "trs_end", "trs_width", "ENST_id")
    
    prt_pos_df <- merge(prt_pos_df, cds_pos_df, on="ENST_id")
    return(prt_pos_df)
}

get_codon <- function(prt_pos_df) {
    # get full cds codon sequence through biomart
    trs_seq_df <- try(getBM(attributes=c('ensembl_transcript_id', 'coding'),
                            filters = 'ensembl_transcript_id',
                            values = prt_pos_df$ENST_id,
                            mart = ensembl))
    # check for output, if query fails produce empty dataframe
    if (length(dim(trs_seq_df)) != 2) {
    print("Failed to get cds from Biomart")
    trs_seq_df <- data.frame(matrix(data=NA, ncol=2, nrow=1))
    }
    colnames(trs_seq_df) <- c("ENST_id", "ENST_seq")
    
    slice_trs_seq <- function(start, end, id){
        s <- NA
        slice_df <- trs_seq_df[(trs_seq_df$ENST_id == id) & (nchar(trs_seq_df$ENST_seq) >= as.numeric(end)), ]
	if(is.data.frame(slice_df) && nrow(slice_df)!=0) {
	l <- base::strsplit(as.character(slice_df$ENST_seq), "")[[1]]
	s <- paste(l[start:end], collapse='')
	}
        return(s)
    }

    prt_pos_df$codon <- mapply(slice_trs_seq, prt_pos_df["trs_start"][,1],
                               prt_pos_df["trs_end"][,1], prt_pos_df["ENST_id"][,1])
    return(prt_pos_df)
}

get_var_pos_in_codon <- function(gnm_pos38, prt_pos_df, edb) {
    trs_pos <- genomeToTranscript(gnm_pos38, edb)
    cds_pos <- transcriptToCds(trs_pos[[1]], edb)
    cds_pos_df <- as.data.frame(cds_pos)
    cds_pos_df <- unique(cds_pos_df[c("start", "end", "width", "names")])
    colnames(cds_pos_df) <- c("var_trs_start", "var_trs_end", "var_trs_width", "ENST_id")
    prt_pos_df <- merge(prt_pos_df, cds_pos_df, "ENST_id")
    prt_pos_df$var_pos_codon <- prt_pos_df$var_trs_start - prt_pos_df$trs_start
    return(prt_pos_df)
}

codonToAA <- function(codon) {
        if (class(codon) != 'character') {
            return("NA")
        }
        return(GENETIC_CODE[codon])}

map_coords <- function(ID, chr_19, pos_19) {
    lifted_coords <- lif_coords(chr_19, pos_19)
    gnm_pos38 <- lifted_coords[[1]]
    if(length(gnm_pos38) == 0){
    print(sprintf("Failed to liftover %s %s %d\n", ID, chr_19, pos_19))
    return()}
    chr_38 <- lifted_coords[[2]]
    pos_38 <- lifted_coords[[3]]
    edb <- lifted_coords[[4]]

    outcome <- map_to_prot(gnm_pos38, edb)
    if(nrow(na.omit(outcome[[1]])) == 0){
    print(sprintf("Failed to map %s to any protein\n", ID))
    return()}
    prt_pos_df <- outcome[[1]]
    prt_pos_df$ID <- rep(c(toString(ID)), each=length(prt_pos_df$ENSP_id))
    prt_pos <- outcome[[2]]
   
    outcome <- get_AA_seq(prt_pos_df, edb)
    if(is.data.frame(outcome) && nrow(na.omit(outcome))==0) {
    print(sprintf("Failed to obtain any protein sequence for %s in variant %s", prt_pos_df$ENSP_id, ID))
    return()}
    prt_pos_df <- outcome

    prt_pos_df$AA <- mapply(get_AA, prt_pos_df$end, prt_pos_df$prt_seq)
    outcome <- map_to_codon(prt_pos, prt_pos_df, edb)
    if(is.data.frame(outcome) && nrow(na.omit(outcome))==0) {
    print(sprintf("Failed to map %s position %s to a cds codon in variant %s", prt_pos_df$ENSP_id, prt_pos_df$start, ID))
    return()}
    prt_pos_df <- outcome

    outcome <- get_codon(prt_pos_df)
    if(is.data.frame(outcome) && nrow(na.omit(outcome))==0) {
    print(sprintf("Failed to obtain any codon sequence for %s %s %s in variant %s", prt_pos_df$ENST_id, prt_pos_df$trs_start, prt_pos_df$trs_end, ID))
    return()}
    prt_pos_df <- outcome

    outcome <- get_var_pos_in_codon(gnm_pos38, prt_pos_df, edb)
    if(is.data.frame(outcome) && nrow(na.omit(outcome))==0) {
    print(sprintf("Failed to map variant %s to position in codon", ID ))
    return()}
    prt_pos_df <-outcome
    prt_pos_df$AA_check <- unlist(lapply(prt_pos_df$codon, codonToAA))
    
    out_cols <- c("ID","ENST_id","ENSP_id","start","AA","trs_start",
		  "trs_end","trs_width","codon","AA_check","var_pos_codon")
    check_cols <- is.element(out_cols, colnames(prt_pos_df)) 
    if(FALSE %in% check_cols){
    print(sprintf("Missing column(s) in output"))
    return()}
    
    out_df <- prt_pos_df[out_cols]
    write.table(out_df, args[3], sep = "\t", col.names = FALSE, 
                row.names = FALSE, append = TRUE)
    return()
}

mapply(map_coords, data["ID"][,1], data["CHROM"][,1], data["POS"][,1])
